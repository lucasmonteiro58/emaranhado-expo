export const depoimentos = [
  {
    text: "Tá muito show!!!!\n",
    id: 15,
  },
  {
    text: "Amei! Refletir para contribuir da melhor maneira possível! Parabéns!",
    id: 16,
  },
  {
    text: "Sabe aquele emoticon da cabeça explodindo? Assim que eu estou! Incrível é pouco pra descrever essa experiência! Ainda estou descobrindo cada cantinho!! <3",
    id: 17,
  },
  {
    text: "A exposição está incrível, o tema,  as obras, os questionamentos... Parabéns a toda equipe de curadores!!",
    id: 18,
  },
  {
    text: "Emaranhei-me! Parabéns! :-)",
    id: 19,
  },
  {
    text: "Achei muito legal a condução e as diversas facetas dessa nova cultura virtual!! Parabéns !!",
    id: 20,
  },
  {
    text: "Exposição primorosa! Qualidade UFRGS <3\nTemática abordada de forma dinânica e fluida, me senti seduzida pelos textos [pontuais!] e pelo design que não fatiga!\nExperiência singular! Parabéns aos alunes-curadores!",
    id: 21,
  },
  {
    text: "Incrível! Nunca tinha visto uma exposição virtual assim e amei!",
    id: 22,
  },
  {
    text: "Ficou linda a exposição, parabéns! As obras selecionadas são muito impactantes. ",
    id: 23,
  },
  {
    text: "parabéns, gente! tá incrível demais",
    id: 24,
  },
  {
    text: "Exposição maravilhosa! Muito obrigada pelas reflexões que me foram provocadas :) adorei!",
    id: 25,
  },
  {
    text: "Adorei as várias obras e a reflexão crítica sobre o virtual. Parabéns!",
    id: 26,
  },
  {
    text: "Adorei as várias obras e a reflexão crítica sobre o virtual. Parabéns!",
    id: 27,
  },
  {
    text: "Parabéns! Foi a melhor exposição virtual que já vi!",
    id: 28,
  },
  {
    text: "Nossa! Achei incrivel demais. Site muito bem feito! Parabéns!",
    id: 29,
  },
];
