export const videos = {
  paula: require("../assets/images/nucleo1/video1.mp4"),
  bug_milenio: require("../assets/images/nucleo2/video1.mp4"),
  blue: require("../assets/images/nucleo2/video4.mp4"),
  blip: require("../assets/images/nucleo2/video5.mp4"),
  coracao: require("../assets/images/nucleo2/video3.mp4"),
  destroi: require("../assets/images/nucleo2/video7.mp4"),
  aura: require("../assets/images/nucleo2/video6.mp4"),
};
