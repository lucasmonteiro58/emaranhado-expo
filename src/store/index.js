import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    imageFull: false,
    image: "",
    audio: false,
  },
  mutations: {
    openFullImage(state, image) {
      console.log(image);
      state.image = image;
      state.imageFull = true;
    },
    closeFullImage(state) {
      state.imageFull = false;
    },
    playAudio(state) {
      state.audio = true;
    },
    stopAudio(state) {
      state.audio = false;
    },
  },
  actions: {},
  modules: {},
});
