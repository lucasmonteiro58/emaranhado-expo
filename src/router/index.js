import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Educativo from "../views/Educativo.vue";
import Programacao from "../views/Programacao.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/educativo",
    name: "educativo",
    component: Educativo,
  },
  {
    path: "/programacao",
    name: "programacao",
    component: Programacao,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
