import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

import AOS from "aos";
import "aos/dist/aos.css";

import "../src/assets/styles/typografia.css";

import KsVueScrollmagic from "ks-vue-scrollmagic";
Vue.use(KsVueScrollmagic);

//tooltip
import VTooltip from "v-tooltip";
Vue.use(VTooltip);

import VueScrollProgressBar from "@guillaumebriday/vue-scroll-progress-bar";
Vue.use(VueScrollProgressBar);

// checkbox
import CheckboxRadio from "vue-checkbox-radio";
Vue.use(CheckboxRadio);

//toast

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 20,
  newestOnTop: true,
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
  mounted() {
    AOS.init();
  },
}).$mount("#app");
